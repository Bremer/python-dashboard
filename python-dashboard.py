import pygame
import sys
import time
import subprocess

from shutil import which


def date():
    if which('date'):
        date_msg = subprocess.getoutput('date')
        return date_msg
    else:
        return 'Sorry, date not installed'


def uptime():
    if which('uptime'):
        uptime_msg = subprocess.getoutput('uptime')
        return uptime_msg
    else:
        return 'Sorry, uptime not installed'


if __name__ == "__main__":

    pygame.init()
    size = width, height = pygame.display.Info().current_w, pygame.display.Info().current_h
    black = 0, 0, 0

    screen = pygame.display.set_mode(size)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()

        font = pygame.font.Font("freesansbold.ttf", 50)
        date_msg = date()
        uptime_msg = uptime()
        date_label = font.render(date_msg, 1, (255,100,0))
        uptime_label = font.render(uptime_msg, 1, (255,100,0))
        pygame.display.update()

        screen.fill(black)
        screen.blit(date_label, (100, 300))
        screen.blit(uptime_label, (100, 600))
        pygame.display.flip()
        time.sleep(30)
